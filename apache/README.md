Qua la parte di progetto con Apche2 dove sono bloccato.
A fine di test, ho realizzato sia un dockerfile che un docker-compose.
L'idea di base è di utilizzare esclusivamente il compose.
Di nuovo, come in precedenza, facendo la combinazione dockerfile e docker-compose il container si ferma.
Ho provato a tenere più scarno possibile il contenuto dei due file seguendo varie indicazioni, senza successo purtroppo.
I file di configurazione sono stati creati ed inseriti all'interno del container 
seguendo le indicazioni della pagina ufficiale dell'immagine su DockerHub e varie soluzioni proposte su StackOverflow.

