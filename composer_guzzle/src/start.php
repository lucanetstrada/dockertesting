<?php
require __DIR__ . '/vendor/autoload.php';


use GuzzleHttp\Client;

// Create a client with a base URI
$client = new GuzzleHttp\Client(['base_uri' => 'https://jsonplaceholder.typicode.com/']);
$response = $client->request('GET', 'todos/1');

echo var_export($response->getBody());